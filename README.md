### linux（centos7）安装ftp服务器并使用java进行简单的文件上传和下载

#### ftp安装

服务器：操作系统centos 7
安装命令： 有的服务器可能没有安装yum，需要先下载安装yum

```bash
yum -y install vsftpd
```

默认安装目录是/etc/vsftpd



然后修改配置文件，目录是/etc/vsftpd/vsftpd.conf

```bash
vim /etc/vsftpd/vsftpd.conf
```

anonymous_enable=YES处，改为NO，表示禁止匿名登录，不改也能用，不安全（但是服务器可能会被黑）



设置vsftpd开机启动

```bash
systemctl enable vsftpd.service
```



启动并查看vsftpd服务状态，systemctl启动服务成功不会有任何提示

```bash
systemctl start vsftpd.service
```



查看服务状态，绿色的active表示服务正在运行

```bash
systemctl status vsftpd.service
```



root模式下创建好ftp文件夹

```bash
mkdir -p /xxx
```

修改文件夹读写权限配置

```bash
chmod 777 /xxx
```



注意：进行ftp连接时，不要连接root账户，要连接其他账户

如果没有非root账户可自行创建

```bash
useradd [用户名]
```

设定该用户的密码

```bash
passwd [用户名]
```

输入密码



### java代码部分

基于springboot框架

maven依赖为：

```xml
<dependency>
    <groupId>commons-net</groupId>
    <artifactId>commons-net</artifactId>
    <version>3.6</version>
</dependency>
```

**主要代码目录为：ftpServer\src\main\java\com\mage\ftpserver\util\FtpUtil.java**

直接执行main函数即可